import { useSearchParams } from "react-router-dom";
import { useMemo, useState } from "react";
const uniqueKey = crypto.randomUUID();

const qualityProd = {
  startsWith: "https://digital.login.gov.az",
  url: "https://e-keyfiyyet.gov.az",
  clientId: "1bb033c2aee64a5fbf639ca09318b154",
};

const qualityTest = {
  startsWith: "https://portal.login.gov.az",
  url: "https://test.e-keyfiyyet.gov.az",
  clientId: "bcb8a950ce2d4742b39ab24e159af3b4",
};

const competitionProd = {
  startsWith: "https://digital.login.gov.az",
  url: "https://e-reqabet.gov.az",
  clientId: "1bb033c2aee64a5fbf639ca09318b154",
};

const competitionTest = {
  startsWith: "https://portal.login.gov.az",
  url: "https://test.e-reqabet.gov.az",
  clientId: "44e01fd51e5e4c1a9f6b34a990e149c2",
};

const local = {
  startsWith: "https://portal.login.gov.az",
  url: "http://localhost:5901",
  clientId: "fb541713acd54641b4e24b3724ca811e",
};

const ttkfMeisTest = {
  startsWith: "https://portal.login.gov.az",
  url: "https://mis.e-ttkf.edu.az",
  clientId: "2bfd94d461734d4eac61457d8e0204da",
};

const gis = {
  startsWith: "https://portal.login.gov.az",
  url: "http://192.168.173.129:81/login",
  clientId: "0468b604f67248d48186762a913f0255",
};

const buttons = [qualityProd, qualityTest, competitionProd, competitionTest, local, ttkfMeisTest, gis];

function App() {
  const [searchParams] = useSearchParams();
  const localStorageKey = localStorage.getItem("uniqueKey");
  const code = searchParams.get("code");
  const [details, setDetails] = useState(qualityProd);
  const state = searchParams.get("state");

  const onClick = function (details: typeof qualityTest) {
    setDetails(details);
  };

  const copyUrl = function () {
    navigator.clipboard.writeText(generatedUrl);
    alert("Coppied ✅");
  };

  const generatedUrl = useMemo(() => {
    const uniqueKey = crypto.randomUUID();
    return `${details.startsWith}/grant-permission?client_id=${details.clientId}&redirect_uri=${details.url}&response_type=code&state=${uniqueKey}&scope=openid certificate session`;
  }, [details]);

  return (
    <div className="main-wrapper">
      <div style={{ fontSize: 24 }}>{state === localStorageKey ? "Success: ✅" : "Failure: ❌"}</div>

      <div
        style={{
          marginTop: "1rem",
          display: "flex",
          gap: "1rem",
          marginBottom: 16,
          width: "50vw",
          flexWrap: "wrap",
        }}
      >
        {buttons.map((button) => {
          return (
            <button
              style={{ padding: "0.5rem 1rem", borderRadius: "0.25rem", border: "none", cursor: "pointer" }}
              key={button.url}
              onClick={() => onClick(button)}
            >
              {button.url}
            </button>
          );
        })}
      </div>

      <h5>selected: {JSON.stringify(details)}</h5>
      <h5 style={{ padding: 40 }}>generated url:</h5>
      <div className="copy-wrapper">
        <input onClick={copyUrl} type="text" value={generatedUrl} />
        <button onClick={copyUrl}>Copy</button>
      </div>
      {code && <h6>Send to backend: {code}</h6>}

      <a style={{ margin: 16 }} onClick={() => localStorage.setItem("uniqueKey", uniqueKey)} href={generatedUrl}>
        Directly login
      </a>
    </div>
  );
}

export default App;
